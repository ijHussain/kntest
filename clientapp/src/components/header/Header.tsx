import * as React from 'react'
import {HeaderStyled} from "./styledHeader"

const Logo = require("./logo.png")
const Header = (props: any) => (
    <header className="row">
        <HeaderStyled.Logo>
            <img src={Logo} alt="logo"/>
        </HeaderStyled.Logo>
        <HeaderStyled.Slogan>
            {props.slogan}
        </HeaderStyled.Slogan>
    </header>

)
export default Header
