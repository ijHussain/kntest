import styled from "styled-components"

const Logo = styled.div`
height: auto
`
const Slogan = styled.div`
    font-style: italic;
    color: #0068a8;
    font-size:14px
`

export const HeaderStyled = {
    Logo, Slogan
}
