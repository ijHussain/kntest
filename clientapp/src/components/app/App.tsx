import * as React from 'react'
import Header from '../header/Header'
import { Link, Switch, Route } from 'react-router-dom'
import FileUploader from "../uploader/fileUploader"
import DownLoader from "../downloader/downLoader"
import {fileService} from "../../services/FileServices"
import Welcome from "../welcome/welcome"
import Search from "../search/search"
class App extends React.Component<any, any> {
    constructor (props: any)
    {super(props)
    this.state = {
        fileList : []
    }
    }

    async componentDidMount() {
        await this.getData()
    }

    getData = () => {
        const jsonData = fileService.getCustomerFiles()
        jsonData.then((data) => {
            this.setMyState(data)
        }).catch(()=> this.setMyState([]))
    }

    setMyState = (fileList: any) => {
        this.setState({fileList : fileList})
    }

    render () {
        return (

            <div className="container">
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <div id="navbarNavAltMarkup">
                        <ul className="nav nav-pills nav-fill">
                            <li className="nav-item">  <Link className="nav-link" to='/'>Home</Link> </li>
                            <li className="nav-item">  <Link className="nav-link" to='/upload'>Upload File</Link> </li>
                            <li className="nav-item"><Link className="nav-link" to='/download'>Download Files ({this.state.fileList.length})</Link> </li>
                            <li className="nav-item"> <Link className="nav-link" to='/search'>Search File</Link></li>
                        </ul>
                    </div>
                </nav>
                <Header slogan={"Slogan goes here."} />
                <Switch>
                    <Route exact path='/' render={(props) => <Welcome customer={"Hello World"} />} />
                    <Route path='/upload' render={(props) => <FileUploader refreshList = {this.getData} />} />
                    <Route path='/download' render={(props) => <DownLoader files={this.state.fileList}  />} />
                    <Route path='/search' render={(props) => {
                        if (!this.state.fileList.length) {
                            return <h3>Opps..File List is empty</h3>
                        }
                        return <Search files={this.state.fileList}  />}
                    }></Route>

                    {/*<Route render={() => <h1>Booom! path not found!</h1>}></Route>*/}
                </Switch>
            </div>
        )
    }

}

export default App
