import * as React from 'react'

const Welcome = (props: any) => (
    <div className="d-flex flex-row">
        <div className="p-2">
            <h4>Find your logistics contacts</h4>
            <h5>Hoia oma müükide edenemisel silma peal</h5>
            <p>We offer a Global Logistics Network with worldwide offices and distribution facilities, staffed by dedicated teams of experts..</p>
        </div>
        <div className="p-2">
            <h4>Kühne + Nageli lennutranspordi teenuste komplekt</h4>
            <h5>Meie väärtusepõhised, spetsiaalsed lahendused on kujundatud vastavalt Teie transpordivajadustele</h5>
            <p>Meie kõiki lisasid sisaldava hinna puhul, mis sisaldab ka kütuse- ja julgeoleku lisatasusid,
                lisaks automaatne arvete väljastamine, saate arve pärast täpselt samas summas kui algne hinnapakkumine..</p>
        </div>
    </div>

)
export default Welcome

