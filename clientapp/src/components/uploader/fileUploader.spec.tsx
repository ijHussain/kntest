import * as React from "react"
import { shallow ,configure } from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'
import FileUploader from "./fileUploader"

configure({ adapter: new Adapter() })

const clickHandler = (d: any) => {
// emulate the click event.
}

describe("File Uploader", () => {

    it("should render correctly", () => {
        const wrapper = shallow(<FileUploader />)
        expect(wrapper.length).toEqual(1)
    })

    it("Component should have one inputFile", () => {
        const wrapper = shallow(<FileUploader />)
        const loader = wrapper.find("input")
        expect(loader).toHaveLength(1)
    })

    it("Component render props correctly", () => {
    // pending..
        expect(1).toEqual(1)
    })


    it("File Uploader should Call parent function to refresh the upload list", () => {
        // pending..
        expect(1).toEqual(1)
    })

    // more to come.
})
