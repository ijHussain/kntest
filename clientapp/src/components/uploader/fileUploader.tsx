import * as React from "react"
import {fileService} from "../../services/FileServices"

interface State {
    file: any,
    message: string,
    rolling: string
}

// assumg reading translation from another services..
const msg = {
    welcome: "Please choose an xml file and click on upload  now button",
    readytoGo: "Ready to go, Click on Send Button",
    selectFile: "Please select a file and click on upload button",
    progress: "Please Wait.. File is in progress...",
    success: "file uploaded and update is in progress pls wait for a sec. aitah",
    error: "In case of error pls contact to sysAdmin",
    done: "Thanks, List updated!"
}


class FileUploader extends React.Component<any, State> {
    constructor(props: any) {
        super(props)
        this.state = {
            file: null,
            message: msg.welcome,
            rolling: "text-info"
        }
    }

    setFile = (e: any) => {
        const selected = e.target.files[0]
        if (selected) {
            this.setState({file: selected, message: msg.readytoGo, rolling: "text-primary"})

        } else {
            this.setState({file: selected, message: msg.selectFile, rolling: "text-info"})

        }
    }

    uploadFile = (e: any) => {
        if (!this.state.file) {
            this.setState({message: msg.welcome})
            return
        }
        const formData = new FormData()
        formData.append('file', this.state.file)
        this.setState({message: msg.progress})

        fileService.saveUploadedFile(formData)
            ? this.setState({message: msg.success, rolling: "text-warning"})
            : this.setState({message: msg.error, rolling: "text-danger"})

        setTimeout(() => {
            this.props.refreshList()
            this.setState({message: msg.done, rolling: "text-success"})
        }, 1000)
    }

    render() {
        return (
            <div className="d-flex flex-sm-col flex-column">
                <div className="alert alert-primary" role="alert">
                    <a href="#" className="alert-link">File Uploader</a>
                </div>
                <div className="mr-auto p-2"><input type="file" accept=".xml" onChange={this.setFile}
                                                    className="form-control-file" id="uploadField" name="uploadField"/>
                </div>
                <div className="mr-auto p-2"><p className={this.state.rolling}> {this.state.message} </p></div>
                <div className="p-2">
                    <button type="button" onClick={this.uploadFile} id="uploadButton" name="uploadButton"
                            className="btn btn-dark">Upload Now!
                    </button>
                </div>
            </div>
        )
    }
}

export default FileUploader
