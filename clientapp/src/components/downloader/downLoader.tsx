import * as React from "react"
import Paging from "../../components/paging/Paging"

interface State {
    file: any,
    message: string,
    rolling: string
}


class DownLoader extends React.Component<any> {

    componentWillReceiveProps(nextProps: any) {
        this.setState({test: nextProps.data});
    }

    constructor(props: any) {
        super(props)
        this.state = {
            date: null
        }
    }

    componentDidMount() {
        this.setState({data: this.props.files})
    }

    render() {
        return (
            <div>
                <div className="alert alert-info" role="alert"><a href="#" className="alert-link">Downloads File</a>
                </div>
                <div className="container-fluid">

                    <div className="row">
                        <table id="profileTable" className="col-sm-12 table table-striped">
                            <thead className="bg-info">
                            <tr>
                                <th scope="col">File Id</th>
                                <th scope="col">File Name</th>
                                <th scope="col">Download</th>
                            </tr>
                            </thead>
                            <tbody>

                            {this.props.files.map((file: any) =>
                                <tr key={file.id}>
                                    <td> {file.id}</td>
                                    <td>{file.fileName} </td>
                                    <td><a href={file.filePath} className="nav-link" target="_blank">{file.fileName}</a>

                                    </td>
                                </tr>
                            )}
                            <tr>
                                <td colSpan={5}> <Paging/></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}

export default DownLoader
