import * as React from 'react'

interface State {
    filteredList: [],
    detail: any,
}

// it is good to have separate folder for interfaces and props.
class Search extends React.Component<any, State> {
    constructor(props: any) {
        super(props)
        this.state = {
            filteredList: [],
            detail: null,
        }
    }

    filterData = () => {
        console.log('pending');
    }

    render() {
        //  let detail = null => to show the filter list.. pending
        return (
            <div>
                <div className="row">
                    <div className="input-group mb-3">
                        <input type="text" className="form-control" placeholder="File Name to Search.."/>
                        <div className="input-group-append">
                            <button className="btn btn-primary" onClick={this.filterData} type="button">Go</button>
                            <button className="btn btn-danger" type="button">Clear</button>
                        </div>
                    </div>
                     <div className="alert alert-dark" role="alert">
                        Search feature is pending!
                    </div>
                </div>
            </div>
        )
    }
}

export default Search
