import * as React from 'react'
import * as ReactDOM from 'react-dom'
import App from './components/app/App'
import {BrowserRouter} from 'react-router-dom'
import {injectGlobal} from "styled-components"

ReactDOM.render(
    <BrowserRouter>
        <App/>
    </BrowserRouter>,
    document.getElementById('root') as HTMLElement
)

injectGlobal`
body {
text-align: initial;
    font-size: medium;
}

* {
    font-family: Open Sans,sans-serif;
}
`
