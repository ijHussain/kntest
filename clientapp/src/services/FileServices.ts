class FileService {

    constructor() {
    }

    public async getCustomerFiles() {

        return await fetch("/v1/api/file", {
            method: "GET",
            headers: {"Content-Type": "application/json; charset=utf-8"},

        }).then(resp => resp.json())
            .then(data => data)
    }

    public saveUploadedFile(file: any): boolean {
        fetch("/v1/api/file/", {
            method: 'POST',
            body: file,
        }).then(res => res.json())
            .then(() => {
                return true
            })
            .catch((e) => {
                console.log(e)
            })
        return false
    }
}

export const fileService = new FileService()
