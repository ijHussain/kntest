export  default  interface IFileUploaderServices {
    setFile () : void;
    getFiles (userId : number) : void;
}
