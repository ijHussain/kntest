module.exports = {
  setupFiles: ['<rootDir>/enzyme.config.js'], // Needed to not get an error on jsdom (https://github.com/jsdom/jsdom/issues/2304#issuecomment-408320484)
  verbose: true,
  collectCoverage: true, // Indicates whether the coverage information should be collected while executing the test
  collectCoverageFrom: [
    `src/**/*.{tsx,ts}`,
      "!src/index.ts",
    "!**/*.d.ts",
    "!**/node_modules/**",
  ],
  coverageThreshold: { // http://facebook.github.io/jest/docs/en/configuration.html#coveragethreshold-object
    global: {
      branches: 85,
      functions: 85,
      lines: 85,
      statements: 85
    }
  },
  transform: {
    "^.+\\.tsx?$": "ts-jest"
  },
  
  moduleFileExtensions: [
    "ts",
    "tsx",
    "js",
    "jsx",
    "json"
  ],
  setupTestFrameworkScriptFile: "./enzyme.config.js",
  snapshotSerializers: ["enzyme-to-json/serializer"],
};